﻿using System.Reflection;
using DtoAbstractLayer;
using LibraryDTO;
using Microsoft.OpenApi.Models;
using MyLibraryManager;
using OpenLibraryClient;
using StubbedDTO;

var dto = System.Environment.GetEnvironmentVariable("DTO", System.EnvironmentVariableTarget.Process);


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

switch(dto)
{
    case "WRAPPER":
        builder.Services.AddSingleton<IDtoManager, OpenLibClientAPI>();
        break;
    case "DATABASE":
        string db_server = "https://codefirst.iut.uca.fr/containers/felixmielcarek-databaseopenlibrary";
        string db_name = (string)System.Environment.GetEnvironmentVariable("MARIADB_DATABASE", System.EnvironmentVariableTarget.Process);
        string db_user = (string)System.Environment.GetEnvironmentVariable("MARIADB_USER", System.EnvironmentVariableTarget.Process);
        string db_password = (string)System.Environment.GetEnvironmentVariable("MARIADB_PASSWORD", System.EnvironmentVariableTarget.Process);

        string dbParams = $"server={db_server};port=3306;database={db_name};user={db_user};password={db_password};";
        
        builder.Services.AddSingleton<IDtoManager, MyLibraryMgr>(
            x=> new MyLibraryMgr(dbParams)
        );
        break;
    default:
        builder.Services.AddSingleton<IDtoManager, Stub>();
        break;
}

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
    {
        options.SwaggerDoc(
            "v1", 
            new OpenApiInfo 
            { 
                    Title = "My Open Library API", 
                    Version = "v1",
                    Description="A Web API for managing books, authors and works"
            });
    });

var app = builder.Build();

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseSwagger();

app.UseSwaggerUI(options =>
{
    options.SwaggerEndpoint("/docs/swagger.json", "v1");
});

app.MapControllers();

app.Run();